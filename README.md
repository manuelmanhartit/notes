# Notes 1.0

This is my cloud note taking app. Its based on [trilium][1] for now, I am currently testing its usefulness use it as a knowledge base (based on the [Zettelkasten method][5]), as TODO list / daily / weekly agenda and whatever it might be useful for.

Alternative selfhosted software bases are [connectednotes][2] (works only with Google Drive), [Organice][3] (Google Drive or Dropbox), [Emanote (formerly Neuron)][4].

If you have more generic questions please have a look at the [Docker Image][1].

## Getting Started

For starting the service initially just copy the `.env-example` to `.env` and edit the variables so they fit your environment.

For the handling of the notepad itself please look into the documentation of the software this is based on (see above).

### Upgrading the service / containers

For upgrading the container, you will need to call

	# docker-compose down; docker-compose pull; docker-compose up -d

If you put in your dyndns settings in another way then via `.env` please ensure that it still works with the new container.

### Backup

Please do a backup of the `./data` folder, or the corresponding if you changed it in your `.env` file.

## About the project

### Versioning

We use MAIN.MINOR number for versioning where as MAIN usually means big / breaking changes and MINOR usually means small / non breaking changes. For the version number, see at the top header or in the [tags on this repository][10].

### Author(s)

* **Manuel Manhart** - *Initial work*

### License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

### Contributing

If you want to contribute, contact the author or create a pull request.

### Changelog

__1.0__

* Created the project
* Created a new bitbucket repo for the container project

### Open issues

* No support for multiple users - The current note software will probably never support this

## Sources

* [Trilium][1]
* [Connectednotes][2]
* [Organice][3]
* [Emanote][4]
* [Zettelkasten Method][5]
* [Docker][6] - The container base
* [Docker-Compose][7] - Composing multiple containers into one service
* [VS Code][8] - Used to edit all the files
* [Project Repo][9] - Link to the git page of the project
* [Project Versions][10] - Link to the tags git page of the project


[1]: https://github.com/zadam/trilium
[2]: https://github.com/tsiki/connectednotes
[3]: https://organice.200ok.ch/sample
[4]: https://github.com/srid/emanote
[5]: https://zettelkasten.de/tools/
[6]: http://www.docker.io/
[7]: https://docs.docker.com/compose/
[8]: https://code.visualstudio.com/
[9]: https://bitbucket.org/mmprivat/notes
[10]: https://bitbucket.org/mmprivat/notes/downloads/?tab=tags